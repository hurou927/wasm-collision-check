
const fs = require('fs')
const {createCanvas, loadImage, Image} = require('canvas');

const canvas_w = 2000;
const canvas_h = 2000;


const getWAInstance = async (filepath, importObject) => {
  const bytes = fs.readFileSync(filepath)
  return await WebAssembly.instantiate(new Uint8Array(bytes), importObject)
}

const filename = "data_structures.wasm";

const main = async () => {
  const memory = new WebAssembly.Memory({initial: 1})
  const mem_i32 = new Uint32Array(memory.buffer);


  const obj_base_addr = 0;
  const obj_count = 32;
  const obj_stride = 16;
  const x_offset = 0;
  const y_offset = 4;
  const radius_offset = 8;
  const collision_offset = 12;

  const obj_i32_base_index = obj_base_addr / 4;
  const obj_i32_stride = obj_stride / 4;

  const x_offset_i32 = x_offset / 4;
  const y_offset_i32 = y_offset / 4;
  const radius_offset_i32 = radius_offset / 4;
  const collision_offset_i32 = collision_offset / 4;



  const importObject = {
    env: {
      mem: memory,
      obj_base_addr: obj_base_addr,
      obj_count: obj_count,
      obj_stride: obj_stride,
      x_offset: x_offset,
      y_offset: y_offset,
      radius_offset: radius_offset,
      collision_offset: collision_offset,
    }
  }


  for (let i = 0; i < obj_count; i++) {
    const index = obj_i32_stride * i + obj_i32_base_index;
    const max_radius = 150;
    let x = Math.floor(Math.random() * (canvas_w - max_radius * 2)) + max_radius;
    let y = Math.floor(Math.random() * (canvas_h - max_radius * 2)) + max_radius;
    let r = Math.floor(Math.random() * max_radius);

    mem_i32[index + x_offset_i32] = x;
    mem_i32[index + y_offset_i32] = y;
    mem_i32[index + radius_offset_i32] = r;
  }

  await getWAInstance(__dirname + "/" + filename, importObject)

  for (let i = 0; i < obj_count; i++) {
    const index = obj_i32_stride * i + obj_i32_base_index;
    const i_str = i.toString().padStart(2, '')
    const x = mem_i32[index + x_offset_i32].toString().padStart(2, '')
    const y = mem_i32[index + y_offset_i32].toString().padStart(2, '')
    const r = mem_i32[index + radius_offset_i32].toString().padStart(2, '')
    const c = !!mem_i32[index + collision_offset_i32]
    console.log(`obj[${i_str}]. x: ${x}, y: ${y}, r: ${r}, collision: ${c}`)
  }
  const canvas = createCanvas(canvas_w, canvas_h, 'png')

  const ctx = canvas.getContext('2d')
  ctx.beginPath();
  ctx.fillStyle = 'rgb( 255, 255, 255)';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  // ctx.beginPath();
  for (let i = 0; i < obj_count; i++) {
    const index = obj_i32_stride * i + obj_i32_base_index;
    const x = mem_i32[index + x_offset_i32]
    const y = mem_i32[index + y_offset_i32]
    const r = mem_i32[index + radius_offset_i32]
    const c = !!mem_i32[index + collision_offset_i32]
    ctx.beginPath()
    ctx.arc(x, y, r, 0, Math.PI * 2, false); // outer (filled)
    if (c) {
      ctx.fillStyle = 'rgb(0,255,255)'
      ctx.fill()
    } else {
      ctx.stroke()
    }
  }
  for (let i = 0; i < obj_count; i++) {
    const index = obj_i32_stride * i + obj_i32_base_index;
    const x = mem_i32[index + x_offset_i32]
    const y = mem_i32[index + y_offset_i32]
    const i_str = i.toString().padStart(2, '')
    ctx.fillStyle = 'rgb(0,0,0)'
    ctx.font = '60px Ubuntu Mono'
    ctx.fillText(i_str, x-10, y-10)
  }
  fs.writeFileSync('out.png', canvas.toBuffer())
}


(async () => {
  await main();
})();
